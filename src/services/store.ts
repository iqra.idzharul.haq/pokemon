import { createContext, useContext } from "react";
import { action, observable } from "mobx";

export class Store {
  @observable currentPage = 0;
  @observable selectedType = "0";
  @observable pokemonListByType: any[] = [];

  @action
  setCurrentPage(pageNumber: number) {
    this.currentPage = pageNumber;
  }

  @action
  setSelectedType(selectedType: string) {
    this.selectedType = selectedType;
  }

  @action
  setPokemonListByType(pokemonListByType: any[]) {
    this.pokemonListByType = pokemonListByType;
  }
}
const StoreContext = createContext<Store>({} as Store);

export const StoreProvider = StoreContext.Provider;

export const useStore = (): Store => useContext(StoreContext);
