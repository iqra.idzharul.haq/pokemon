import React, { useState, useEffect } from "react";
import axios from "axios";
import {
  PokemonProfile,
  PokedexNavigation,
  PokemonImage,
  PokemonStats,
} from "../components";

export default function Details(props: any) {
  const pokemonIndex = props.match.params.id;
  const [pokemon, setPokemon] = useState<any>({});
  const [pokemonSpecies, setPokemonSpecies] = useState<any>({});

  useEffect(() => {
    getPokemonDetails(pokemonIndex);
    getPokemonSpecies(pokemonIndex);
  }, [pokemonIndex]);

  const getPokemonDetails = async (pokemonIndex: string) => {
    try {
      const resData = await axios.get(
        `${process.env.REACT_APP_API_ENDPOINT}pokemon/${pokemonIndex}`
      );
      if (resData.status === 200) {
        setPokemon(resData.data);
      } else {
        console.log(
          `error when trying to get pokemon data with status error: ${resData.status}`
        );
      }
    } catch (e) {
      console.log(
        `unknown error when trying to get pokemon data with error: ${e}`
      );
    }
  };

  const getPokemonSpecies = async (pokemonIndex: string) => {
    try {
      const resData = await axios.get(
        `${process.env.REACT_APP_API_ENDPOINT}pokemon-species/${pokemonIndex}`
      );
      if (resData.status === 200) {
        setPokemonSpecies(resData.data);
      } else {
        console.log(
          `error when trying to get pokemon data with status error: ${resData.status}`
        );
      }
    } catch (e) {
      console.log(
        `unknown error when trying to get pokemon data with error: ${e}`
      );
    }
  };

  return (
    <div className="animated fadeIn">
      <PokedexNavigation pokemon={pokemon} pokemonIndex={pokemonIndex} />
      <div className="row justify-content-center p-1-5-0-5">
        <div className="col-md-6 col-sm-12 text-align-center p-4">
          <PokemonImage pokemonIndex={pokemonIndex} />
        </div>
        <div className="col-md-6 col-sm-12 p-4">
          <PokemonProfile pokemon={pokemon} pokemonSpecies={pokemonSpecies} />
        </div>
      </div>
      <div className="row justify-content-center p-0-5-5-5">
        <div className="col text-align-center">
          <PokemonStats pokemon={pokemon} />
        </div>
      </div>
    </div>
  );
}
