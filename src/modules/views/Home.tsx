import React, { useState, useEffect, useCallback } from "react";
import { PokemonList, PokemonSearch, Pagination } from "../components";
import axios from "axios";
import { useStore } from "../../services/store";
import { observer } from "mobx-react-lite";

const Home = observer((props: any) => {
  const store = useStore();
  const [pokemonData, setPokemonData] = useState({
    count: 0,
    next: "",
    previous: "",
    results: [] as any[],
  });
  const [typeList, setTypeList] = useState<any>([]);

  const getDataPokemon = useCallback(
    async (pageNumber: number, limit: number) => {
      try {
        if (store.selectedType !== "0") {
          const pokemonListThisPage = store.pokemonListByType.slice(
            pageNumber * 20,
            pageNumber * 20 + 20
          );

          const results = pokemonListThisPage.map((item: any) => {
            return item.pokemon;
          });
          const pokemonData = {
            count: store.pokemonListByType.length,
            previous: "",
            next: "",
            results: results,
          };

          setPokemonData(pokemonData);
          store.setCurrentPage(pageNumber);
        } else {
          const resData = await axios.get(
            `${process.env.REACT_APP_API_ENDPOINT}pokemon?offset=${
              pageNumber * limit
            }&limit=${limit}`
          );
          if (resData.status === 200) {
            setPokemonData(resData.data);
            store.setCurrentPage(pageNumber);
          } else {
            console.log(
              `error when trying to get pokemon data with status error: ${resData.status}`
            );
          }
        }
      } catch (e) {
        console.log(
          `unknown error when trying to get pokemon data with error: ${e}`
        );
      }
    },
    [store]
  );

  useEffect(() => {
    getDataPokemon(store.currentPage, 20);
  }, [getDataPokemon, store]);

  const openDetails = (pokemonIndex: string) => {
    props.history.push(`details/${pokemonIndex}`);
  };

  const getTypesData = async () => {
    try {
      const resData = await axios.get(
        `${process.env.REACT_APP_API_ENDPOINT}type`
      );
      if (resData.status === 200 && resData.data.results) {
        const newList = [{ name: "ALL TYPE" }, ...resData.data.results];
        setTypeList(newList);
      } else {
        console.log(
          `error when trying to get pokemon type list with status error: ${resData.status}`
        );
      }
    } catch (e) {
      console.log(
        `unknown error when trying to get pokemon type list with error: ${e}`
      );
    }
  };

  useEffect(() => {
    getTypesData();
  }, []);

  const setPokemonType = async (selectedType: any) => {
    try {
      store.setSelectedType(selectedType);
      if (selectedType === "0") {
        store.setPokemonListByType([]);
      } else {
        const resData = await axios.get(
          `${process.env.REACT_APP_API_ENDPOINT}type/${selectedType}`
        );
        if (resData.status === 200) {
          store.setPokemonListByType(resData.data.pokemon);
          getDataPokemon(0, 20);
        } else {
          store.setPokemonListByType([]);
          console.log(
            `error when trying to get pokemon type list with status error: ${resData.status}`
          );
        }
      }
    } catch (e) {
      console.log(
        `unknown error when trying to get pokemon type list with error: ${e}`
      );
    }
    getDataPokemon(0, 20);
  };

  return (
    <div className="animated fadeIn px-5">
      <div className="row margin-top-50 justify-content-center">
        <h1>Complete Pokémon Pokédex</h1>
        <div className="max-width-800 text-align-center p-3">
          <p>
            This is a full list of every Pokémon from all 8 generations of the
            Pokémon series.
            <br /> The table is searchable by using the controls above it. Click
            for detailed information about that pokemon.
          </p>
        </div>
      </div>
      <div className="row margin-bot-25 justify-content-center">
        <div className="max-width-800">
          <div className="row justify-content-end p-4">
            <PokemonSearch
              setPokemonType={setPokemonType}
              typeList={typeList}
            ></PokemonSearch>
          </div>
          <div className="row">
            <PokemonList
              openDetails={(pokemoonIndex: string) =>
                openDetails(pokemoonIndex)
              }
              pokemonList={pokemonData.results}
            ></PokemonList>
          </div>
          <div className="row justify-content-center p-4">
            <Pagination
              pokemonData={pokemonData}
              getDataPokemon={(pageNumber: number, limit: number) => {
                getDataPokemon(pageNumber, limit);
              }}
            ></Pagination>
          </div>
        </div>
      </div>
    </div>
  );
});

export default Home;
