import React from "react";

const routes = [
  {
    path: "/",
    exact: true,
    name: "Home",
    component: React.lazy(() => import("./views/Home")),
  },
  {
    path: "/home",
    exact: true,
    name: "Home",
    component: React.lazy(() => import("./views/Home")),
  },
  {
    path: "/details/:id",
    name: "Details",
    component: React.lazy(() => import("./views/Details")),
  },
];

export default routes;
