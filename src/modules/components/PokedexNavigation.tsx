import React, { useEffect, useState } from "react";
import { getThreeDigitNumber, upperCaseFirst } from "../../utilities";
import axios from "axios";
import { useHistory } from "react-router-dom";

export default function PokedexNavigation(props: any) {
  const { pokemon, pokemonIndex } = props;
  const [prevPokemon, setPrevPokemon] = useState<any>({});
  const [nextPokemon, setNextPokemon] = useState<any>({});
  let history = useHistory();

  useEffect(() => {
    getPokemonNavigationDetails(pokemonIndex);
  }, [pokemonIndex]);

  const getPokemonNavigationDetails = async (pokemonIndex: string) => {
    try {
      if (Number.parseInt(pokemonIndex) > 1) {
        const resPrevData = await axios.get(
          `${process.env.REACT_APP_API_ENDPOINT}pokemon/${
            Number.parseInt(pokemonIndex) - 1
          }`
        );
        if (resPrevData.status === 200) {
          setPrevPokemon(resPrevData.data);
        } else {
          console.log(
            `error when trying to get pokemon data with status error: ${resPrevData.status}`
          );
        }
      }
      const resNextData = await axios.get(
        `${process.env.REACT_APP_API_ENDPOINT}pokemon/${
          Number.parseInt(pokemonIndex) + 1
        }`
      );
      if (resNextData.status === 200) {
        setNextPokemon(resNextData.data);
      } else {
        console.log(
          `error when trying to get pokemon data with status error: ${resNextData.status}`
        );
      }
    } catch (e) {
      console.log(
        `unknown error when trying to get pokemon data with error: ${e}`
      );
    }
  };

  return (
    <div>
      <div className="row">
        <div
          className="col-6 pokedex-pokemon-pagination"
          onClick={() =>
            history.push(`/details/${Number.parseInt(pokemonIndex) - 1}`)
          }
        >
          {pokemonIndex > 1 && (
            <div className="pokedex-pokemon-pagination-wrapper">
              <span className="arrow">&nbsp;{"<"}&nbsp;</span>&nbsp;
              <span className="pokemon-number">
                #{getThreeDigitNumber(pokemonIndex - 1)}
              </span>
              &nbsp;
              <span className="pokemon-name hidden-mobile">
                {prevPokemon.name
                  ? upperCaseFirst(prevPokemon.name)
                  : "No Data"}
              </span>
            </div>
          )}
        </div>
        <div
          className="col-6 pokedex-pokemon-pagination text-align-right"
          onClick={() =>
            history.push(`/details/${Number.parseInt(pokemonIndex) + 1}`)
          }
        >
          <div className="pokedex-pokemon-pagination-wrapper">
            <span className="pokemon-name hidden-mobile">
              {nextPokemon.name ? upperCaseFirst(nextPokemon.name) : "No Data"}
            </span>
            &nbsp;
            <span className="pokemon-number">
              #{getThreeDigitNumber(Number.parseInt(pokemonIndex) + 1)}
            </span>
            &nbsp;
            <span className="arrow">&nbsp;{">"}&nbsp;</span>&nbsp;
          </div>
        </div>
      </div>
      <div className="rows justify-content-center">
        <div className="pokedex-pokemon-pagination-title">
          <div>
            <span className="pokemon-number">
              #{getThreeDigitNumber(pokemonIndex)}
            </span>
            &nbsp;
            {pokemon.name ? upperCaseFirst(pokemon.name) : "No Data"}
          </div>
        </div>
      </div>
    </div>
  );
}
