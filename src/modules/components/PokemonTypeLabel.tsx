import React from "react";

export default function PokemonTypeLabel(props: any) {
  const { name } = props;
  const style = `pokemon-type-label type-${name}`;
  return <div className={style}>{name}</div>;
}
