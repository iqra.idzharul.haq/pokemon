import React from "react";
import { getThreeDigitNumber, upperCaseFirst } from "../../utilities";
import { PokemonImage } from ".";

export default function PokemonCard(props: any) {
  const { pokemon, openDetails } = props;

  const splits = pokemon.url.split("/");
  let pokemonIndex = splits[6];
  return (
    <div
      className="card pokemon-card"
      onClick={() => {
        openDetails(pokemonIndex);
      }}
    >
      <PokemonImage pokemonIndex={pokemonIndex} />
      <div className="card-body">
        <p className="card-text">#{getThreeDigitNumber(pokemonIndex)}</p>
        <h5 className="card-title">{upperCaseFirst(pokemon.name)}</h5>
      </div>
    </div>
  );
}
