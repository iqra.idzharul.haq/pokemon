import React from "react";
import { upperCaseFirst } from "../../utilities";

export default function PokemonStats(props: any) {
  const { pokemon } = props;
  return (
    <div>
      <div className="p-0-5-1-5">
        <h4>
          {pokemon.name ? upperCaseFirst(pokemon.name) + "'s" : "Pokemon's"}{" "}
          Base Stats
        </h4>
      </div>
      <div className="row">
        {pokemon.stats &&
          pokemon.stats.map((item: any, index: number) => {
            const colorStyle = "color-" + item.stat.name;
            return (
              <div className="col-md-2 col-sm-4 col-6" key={index}>
                <div className={"card pokedex-pokemon-stats " + colorStyle}>
                  <div className="card-body">
                    <h6 className="card-title">
                      {upperCaseFirst(
                        item.stat.name.replace("special-", "Sp. ")
                      )}
                    </h6>
                    <p className="card-text">{item.base_stat}</p>
                  </div>
                </div>
              </div>
            );
          })}
      </div>
    </div>
  );
}
