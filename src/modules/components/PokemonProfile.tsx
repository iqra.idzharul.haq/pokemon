import React from "react";
import { PokemonTypeLabel } from ".";
import { removeUnusedCharacter, upperCaseFirst } from "../../utilities";

export default function PokemonProfile(props: any) {
  const { pokemon, pokemonSpecies } = props;
  return (
    <div>
      <p className="pokemon-description">
        {pokemonSpecies.flavor_text_entries &&
        pokemonSpecies.flavor_text_entries.length
          ? pokemonSpecies.flavor_text_entries[1]
            ? removeUnusedCharacter(
                pokemonSpecies.flavor_text_entries[1].flavor_text
              )
            : pokemonSpecies.flavor_text_entries[0].flavor_text
          : "No Data"}
      </p>
      <div className="pokemon-info-container margin-top-25 p-4">
        <div className="row">
          <div className="col-md-6 col-sm-12 text-align-left">
            Height
            <p>{pokemon.height / 10} Meter</p>
          </div>
          <div className="col-md-6 col-sm-12 text-align-left">
            Species
            <p>
              {pokemonSpecies.genera && pokemonSpecies.genera.length
                ? pokemonSpecies.genera.map((item: any) => {
                    if (item.language.name === "en") return item.genus;
                    return "";
                  })
                : "No Data"}
            </p>
          </div>
        </div>
        <div className="row">
          <div className="col-md-6 col-sm-12  text-align-left">
            Weight
            <p>{pokemon.weight} Kg</p>
          </div>
          <div className="col-md-6 col-sm-12  text-align-left">
            Abilities
            <p>
              {pokemon.abilities && pokemon.abilities.length
                ? pokemon.abilities.map((item: any, index: number) => {
                    return (
                      <span key={index}>
                        {upperCaseFirst(item.ability.name)}
                        <br />
                      </span>
                    );
                  })
                : "No Data"}
            </p>
          </div>
        </div>
        <div className="row">
          <div className="col-md-6 col-sm-12  text-align-left">
            Gender
            <p>
              {pokemonSpecies.gender_rate === -1
                ? "Unknown"
                : pokemonSpecies.gender_rate === 8
                ? "Female-Only"
                : pokemonSpecies.gender_rate === 0
                ? "Male-only"
                : "Female or Male"}
            </p>
          </div>
          <div className="col-md-6 col-sm-12 text-align-left">
            Habitat
            <p>
              {pokemonSpecies.habitat
                ? upperCaseFirst(pokemonSpecies.habitat.name)
                : "No Data"}
            </p>
          </div>
        </div>
      </div>
      <div className="row p-3">
        {pokemon.types
          ? pokemon.types.map((item: any, index: number) => {
              return (
                <div key={index}>
                  <PokemonTypeLabel name={item.type.name}></PokemonTypeLabel>
                </div>
              );
            })
          : "No Data"}
      </div>
    </div>
  );
}
