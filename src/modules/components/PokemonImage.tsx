import React from "react";
import { getThreeDigitNumber } from "../../utilities";

export default function PokemonCard(props: any) {
  const { pokemonIndex } = props;
  return (
    <img
      className="card-img-top"
      src={`${process.env.REACT_APP_IMAGE_ENDPOINT}${getThreeDigitNumber(
        pokemonIndex
      )}.png`}
      alt="pokemon"
    />
  );
}
