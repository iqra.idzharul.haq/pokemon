import React from "react";
import { useStore } from "../../services/store";
import { observer } from "mobx-react-lite";

const Pagination = observer((props: any) => {
  const store = useStore();
  const { getDataPokemon, pokemonData } = props;

  const onChangePage = (pageNumber: number, limit: number) => {
    getDataPokemon(pageNumber, limit);
  };

  return (
    <nav aria-label="Page navigation example">
      <ul className="pagination justify-content-center">
        {store.currentPage - 1 >= 0 ? (
          <li className="page-item">
            <button
              className="page-link"
              onClick={() => {
                onChangePage(0, 20);
              }}
            >
              {"<<"}
            </button>
          </li>
        ) : (
          <li className="page-item disabled">
            <button className="page-link">{"<<"}</button>
          </li>
        )}
        {store.currentPage - 1 >= 0 ? (
          <li className="page-item">
            <button
              className="page-link"
              onClick={() => {
                onChangePage(store.currentPage - 1, 20);
              }}
            >
              {"<"}
            </button>
          </li>
        ) : (
          <li className="page-item disabled">
            <button className="page-link">{"<"}</button>
          </li>
        )}
        {store.currentPage - 2 >= 0 && (
          <li className="page-item">
            <button
              className="page-link"
              onClick={() => {
                onChangePage(store.currentPage - 2, 20);
              }}
            >
              {store.currentPage - 1}
            </button>
          </li>
        )}
        {store.currentPage - 1 >= 0 && (
          <li className="page-item">
            <button
              className="page-link"
              onClick={() => {
                onChangePage(store.currentPage - 1, 20);
              }}
            >
              {store.currentPage}
            </button>
          </li>
        )}
        <li className="page-item active">
          <button className="page-link" onClick={() => {}}>
            {store.currentPage + 1}
          </button>
        </li>
        {store.currentPage + 1 <= Math.floor(pokemonData.count / 20) && (
          <li className="page-item">
            <button
              className="page-link"
              onClick={() => {
                onChangePage(store.currentPage + 1, 20);
              }}
            >
              {store.currentPage + 2}
            </button>
          </li>
        )}
        {store.currentPage + 2 <= Math.floor(pokemonData.count / 20) && (
          <li className="page-item">
            <button
              className="page-link"
              onClick={() => {
                onChangePage(store.currentPage + 2, 20);
              }}
            >
              {store.currentPage + 3}
            </button>
          </li>
        )}
        {store.currentPage + 1 <= Math.floor(pokemonData.count / 20) ? (
          <li className="page-item">
            <button
              className="page-link"
              onClick={() => {
                onChangePage(store.currentPage + 1, 20);
              }}
            >
              {">"}
            </button>
          </li>
        ) : (
          <li className="page-item disabled">
            <button className="page-link">{">"}</button>
          </li>
        )}
        {store.currentPage + 1 <= Math.floor(pokemonData.count / 20) ? (
          <li className="page-item">
            <button
              className="page-link"
              onClick={() => {
                onChangePage(Math.floor(pokemonData.count / 20), 20);
              }}
            >
              {">>"}
            </button>
          </li>
        ) : (
          <li className="page-item disabled">
            <button className="page-link">{">>"}</button>
          </li>
        )}
      </ul>
    </nav>
  );
});

export default Pagination;
