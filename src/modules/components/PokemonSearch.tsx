import React from "react";
import { observer } from "mobx-react-lite";
import { useStore } from "../../services/store";

export const PokemonSearch = observer((props: any) => {
  const store = useStore();
  const { setPokemonType, typeList } = props;

  return (
    <select
      className="btn"
      value={store.selectedType}
      onChange={(e) => {
        setPokemonType(e.target.value);
      }}
    >
      {typeList.map((item: any, index: number) => {
        return (
          <option className="btn-secondary" key={index} value={index}>
            {item.name.toUpperCase()}
          </option>
        );
      })}
    </select>
  );
});

export default PokemonSearch;
