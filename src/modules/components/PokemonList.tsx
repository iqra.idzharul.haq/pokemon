import React from "react";
import { PokemonCard } from ".";

export default function PokemonList(props: any) {
  const { pokemonList, openDetails } = props;
  return (
    <div className="container-fluid">
      <div className="row mb-3">
        {pokemonList &&
          pokemonList.map((item: any, index: number) => {
            return (
              <div className="col-sm" key={index}>
                <PokemonCard
                  openDetails={(pokemoonIndex: string) =>
                    openDetails(pokemoonIndex)
                  }
                  pokemon={item}
                ></PokemonCard>
              </div>
            );
          })}
      </div>
    </div>
  );
}
