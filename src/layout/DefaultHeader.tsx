import React, { useState } from "react";
import logo from "../assets/img/logo.png";
import axios from "axios";
import { useHistory } from "react-router-dom";

export default function DefaultHeader(props: any) {
  const [pokemonName, setPokemonName] = useState<any>("");
  let history = useHistory();

  const searchPokemon = async (e: any) => {
    e.preventDefault();
    const resData = await axios.get(
      `${
        process.env.REACT_APP_API_ENDPOINT
      }pokemon/${pokemonName.toLowerCase()}`
    );
    if (resData.status === 200) {
      if (resData.data.id) history.push(`/details/${resData.data.id}`);
    } else {
      console.log(
        `error when trying to get pokemon data with status error: ${resData.status}`
      );
    }
  };

  return (
    <div>
      <nav className="navbar justify-content between p-3">
        <div className="navbar-brand">
          <div className="row">
            <div
              onClick={() => {
                history.push("/");
              }}
            >
              <img className="logo pointer" src={logo} alt="logo"></img>
            </div>
            <div
              onClick={() => {
                history.push("/");
              }}
              className="pointer hidden-mobile"
            >
              Home
            </div>
          </div>
        </div>
        <form
          className="form-inline"
          onSubmit={(e) => {
            searchPokemon(e);
          }}
        >
          <input
            className="form-control mr-sm-2"
            type="search"
            placeholder="Search Pokemon"
            aria-label="Search"
            value={pokemonName}
            onChange={(e) => setPokemonName(e.target.value)}
          />
          <button
            className="btn btn-secondary my-2 my-sm-0"
            onClick={searchPokemon}
            type="submit"
          >
            Search
          </button>
        </form>
      </nav>
    </div>
  );
}
